To change the SSL certificates go to the `70-ssl-online` and change the line:

`declare -a domains=("example.com" "www.google.com")` to any domains you wish, e.g.:

`declare -a domains=("example.com" "www.youtube.com" "www.reddit.com")`
if you're seeing duplicate motd remove the `show-motd.sh` from /etc/profile.d

<img src="https://i.imgur.com/ZTVRUo7.png" alt="example" width="400"/>
